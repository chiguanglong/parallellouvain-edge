import x10.util.*;
import x10.lang.*;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;

public class ProfileWriter {

	private var file:File;
	private var printer:Printer;
	
	def this(){
	}

	def this(filename:String) {
		file = new File(filename);
		printer = file.printer();
	}

	def this(filename:String, items:Array[String]) {
		file = new File(filename);
		printer = file.printer();
		val size = items.size - 1;
		for (i in 0..size) {
			if (i!=size) 
				printer.print(items(i));
			else 
				printer.print(items(i) + ",");
		}
		printer.println();
	}

	public def setItems(items:Array[String]) {
		val size = items.size - 1;
		for (i in 0..size) {
			if (i!=size) 
				printer.print(items(i));
			else 
				printer.print(items(i) + ",");
		}
		printer.println();
	}

	public def write(line:String) {
		printer.print(line);
	}

	public def writeln(line:String) {
		printer.println(line);
	}

	public def flush() {
		printer.flush();
	}

	public def close() {
		printer.close();
	}
}
