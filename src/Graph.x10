
import x10.lang.*;
import x10.compiler.*;
import x10.util.*;
import x10.io.File;
import x10.io.FileReader;
import x10.lang.Error;
import x10.util.ArrayList;
import x10.util.List; 
import x10.lang.Math;
import x10.io.FileWriter;
import x10.io.BufferedReader;
import x10.io.Printer;

public class Graph {
    var total_weight:double;
    var nb_nodes:int;
    var nb_nodesP:int; //#nodesOfPlace
    var nb_links:int;
    
    var degrees:Array[int];
    var links:Array[int];
    var links_r:Array[ArrayList[Pair[int,double]]];
    var weights:Array[double];

    var originalDegrees:Array[int];
    var masterP:Array[int];
    var nodeList:Array[int];

    var startIndex:int; //for iterating links and weights
    var deg:int;

    @Native("c++", "GC_get_heap_size()")
    public static native def getGCMemSize():long;

    @Native("c++", "GC_INIT()")
    public static native def initGC():void;
    
    public def this()
    {
        links = new Array[int](1);
        weights = new Array[double](1);
        degrees = new Array[int](1);
        links.clear();
        weights.clear(); 
        degrees.clear();
    }

    public def this( filename:String, filename_w:String, type_file:int) {   
        val distFileReader = new DistributedGraphReader();
        distFileReader.read(filename, 0);
        degrees = distFileReader.degrees;
        links = distFileReader.links;
        weights = distFileReader.weights;

        originalDegrees = distFileReader.originalDegrees;
        masterP = distFileReader.masterP;
        nodeList = distFileReader.nodeList;

        nb_nodes = distFileReader.nb_nodes;
        nb_nodesP = distFileReader.nb_nodesP;
        total_weight = distFileReader.totalWeight;
        distFileReader.clear();

        nb_links = Int.operator_as(total_weight);
        nb_nodes = nb_nodes - 1;
    }

    public def this( filename:String, filename_par:String, 
        parSize:int, type_file:int, maxNode:int, direct:int, do_renumber:boolean ) {
        
        Console.OUT.println("In read file part!!");
        val finput = new File(filename);
        var keys:Array[String];
        var math:Math = new Math();

        //par: store partition info for renumbering
        //n2n: correspond original node number to new node number 
        val par = new Array[ArrayList[int]](parSize);
        var n2n:Array[int];
        n2n = new Array[int](maxNode+1,0);
        // for (var i:int=0; i<=maxNode; i++) {
        for (i in 0..maxNode) {
            n2n(i) = i;
        }

        if(filename_par!=null && parSize > 1){
            val finput_par = new File(filename_par);
            for ( var i:int=0; i<parSize; i++) {
                  par(i) = new ArrayList[int]();
            }

            var index:int = 0;
            var partition:int = 0;
            for ( line in finput_par.lines()) {
                partition = Int.parse(line);
                par(partition).add(index);
                index++;
            }
            index = 0;
            var node:int = 0;
            for ( var i:int=0; i<parSize; i++) {
                partition = par(i).size();
                for(var j:int=0; j<partition; j++){
                    node = par(i)(j);
                    n2n(node) = index;
                    index++;
                }
            }
        }

        links_r = new Array[ArrayList[Pair[int, double]]](maxNode+1);
        for (startIndex = 0; startIndex<=maxNode; startIndex++) {
                links_r(startIndex) = new ArrayList[Pair[int, double]]();
        }

        nb_links = 0;
        for (line in finput.lines()) {
            var src:int;
            var dest:int;
            var weight:double = 1.0;

            keys = line.split(" ");

            if (type_file == 1) {
                src  = Int.parse(keys(0));
                dest = Int.parse(keys(1));
                src  = n2n(src);
                dest = n2n(dest);
                weight = Int.parse(keys(2));    
            } else {
                src = Int.parse(keys(0));
                dest = Int.parse(keys(1));
                src  = n2n(src);
                dest = n2n(dest);
            }
            
            links_r(src).add(new Pair(dest, weight));
            if (direct==1&&src!=dest) {
                links_r(dest).add(new Pair(src,weight));
                nb_links++;
            }

            nb_links++;
        }
        Console.OUT.println("nb_links, links_r.size() = " + nb_links + ", " + links_r.size);
    }

    @Inline @NonEscaping final def weighted_degree( node:int ) 
    {
    //return sum of src node`s linked edge`s weight or the num of linked nodes
    //  val ae = new  AssertionError("Node`s ID not correct!!",(node < nb_nodes));
        var res:double = 0.0;         
        var startIndex:int = degrees(node) - degrees(0);
        var deg:int  = degrees(node+1) - degrees(0);

        for (var i:int = startIndex; i<deg; i++) {
            res += weights(i);
        }

        return res;     
    }

    @Inline @NonEscaping final def nb_selfloops(node:int) {
        //AssertionError("Node`s ID not correct!!",(node < nb_nodes));
        var startIndex:int  = degrees(node);
        var deg:int = degrees(node+1);
 
        for (var i:int = startIndex; i<deg; i++)
        {   
            if (links(i) == node) {
                return weights(i);
            }
        }
        return 0.0;
    }

    @Inline @NonEscaping final def nb_selfloops(newNode:int, nodeBand:int) {
        //AssertionError("Node`s ID not correct!!",(node < nb_nodes));
        var startIndex:int  = degrees(newNode) -degrees(0);
        var deg:int = degrees(newNode+1) - degrees(0);
        val originalNode = newNode + nodeBand;
 
        for (var i:int = startIndex; i<deg; i++)
        {   
            if (links(i) == (originalNode)) {
                    return weights(i);
            }
        }
        return 0.0;
    }

    public def display_binary( filename:String, filename_w:String, type_file:int )
    {
        Console.OUT.println("in display_binary part!!");
        Console.OUT.println("filename, filename_w = " + filename + ", " + filename_w);
        val foutput = new File(filename);
        var fwriter:FileWriter = foutput.openWrite();
        deg = links_r.size;

        //outputs number of nodes
        fwriter.writeInt(deg);

        var tot:int = 0;

        //outputs cumulative degree sequence
        for (startIndex=0; startIndex<deg; startIndex++) {
            tot += links_r(startIndex).size();
            fwriter.writeInt(tot);
        }

        //outputs links
        for (startIndex=0; startIndex<deg; startIndex++) {
            for (var i:int=0; i<links_r(startIndex).size(); i++) {
                var dest:int = links_r(startIndex)(i).first;
                fwriter.writeInt(dest);
            }
        }
        fwriter.close();

        //outputs weights in a separate file
        if (type_file == 1) {
            val foutput_w = new File(filename_w);
            var fwriter_w:FileWriter = foutput_w.openWrite();
            for (startIndex=0; startIndex<deg; startIndex++) 
            {
                for (var i:int=0; i<links_r(startIndex).size(); i++) 
                {
                    var weight:double = links_r(startIndex)(i).second;
                    fwriter_w.writeDouble(weight);
                }
            }
            fwriter_w.close();
        }
    }

    public def display_metis(filename:String, outfile:String, maxNode:int) {
        val file = new File(filename);
        val reader = new BufferedReader(new FileReader(file));

        val foutput = new File(outfile);
        val printer = foutput.printer();

        val links_r = new Array[ArrayList[int]](maxNode+1);
        for (startIndex = 0; startIndex<=maxNode; startIndex++) 
        {
            links_r(startIndex) = new ArrayList[int]();
        }

        for (line in reader.lines()) {
            var src:int;
            var dest:int;

            val keys = line.split(" ");

            src = Int.parse(keys(0));
            dest = Int.parse(keys(1));
            
            links_r(src).add(dest);
            nb_links++;
        }

        printer.println((maxNode+1) + " " + nb_links/2);
        nb_links = 0;
        for (node in 0..maxNode) {
            val deg = links_r(node).size()-1;
            for (neighIndex in 0..deg) {
                val neigh = links_r(node)(neighIndex);
                printer.print(" " + (neigh+1));
            }
            if (node==maxNode) 
                printer.print(" ");
            else 
                printer.println(" ");
        }
    }
    
    public def renumberFile(filename:String, outfile:String, maxNode:int, type_file:int, direct:int) {
        val file = new File(filename);
        val reader = new BufferedReader(new FileReader(file));

        val foutput = new File(outfile);
        val printer = foutput.printer();

        val links_rr = new Array[ArrayList[int]](maxNode+1);
        for (startIndex = 0; startIndex<=maxNode; startIndex++) {
            links_rr(startIndex) = new ArrayList[int]();
        }

        for (line in reader.lines()) {
            var src:int;
            var dest:int;

            val keys = line.split(" ");

            src = Int.parse(keys(0));
            dest = Int.parse(keys(1));
            
            if(links_rr(src).binarySearch(dest) >= 0 ) continue;
            links_rr(src).add(dest);
            links_rr(src).sort();
            nb_links++;

            if (direct==1&&src!=dest) {
                if(links_rr(dest).binarySearch(src) >= 0 ) continue;
                links_rr(dest).add(src);
                links_rr(dest).sort();
                nb_links++;
            }
        }

        nb_nodes = 0;
        val renumber = new Array[int](maxNode+1,-1);
        for (i in renumber) {
        	val deg = links_rr(i).size();
        	if(deg != 0) {
        		renumber(i) = nb_nodes;
        		nb_nodes++;
        	}
        }

    	nb_links = 0;
        for (node in 0..maxNode) {
        	val deg = links_rr(node).size()-1;
            for (neighIndex in 0..deg) {
            	val neigh = links_rr(node)(neighIndex);
            	
                printer.println(renumber(node) + " " + renumber(neigh));
            	nb_links++;
            }
        }

        Console.OUT.println("nodes = " + nb_nodes + ", links = " + nb_links);
    }

    public static def main(args: Array[String]) {
        val timer = new Timer();
        var start:Long = timer.milliTime();
        var fileName:String = args(0);
        //val g = new Graph(fileName,0,9376,48214);
        val finput = new File(fileName);
        val freader = finput.openRead();
        for ( line in finput.lines()) {
            
        }
        var end:Long = timer.milliTime();
        Console.OUT.println("It used "+(end-start)+"ms");
    }
}
