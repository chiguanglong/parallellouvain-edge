import x10.util.*;
import x10.lang.*;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;
import x10.compiler.*;

public class ComDetect 
{
    var profile:boolean = false;
    var humanfriendlylog:boolean = false;
    var heap:boolean = false;
    var filename:String;
    var filename_w:String;
    var it_random:int = 1; //0: randomly iterate, non-zero: sequencially iterate 
    var precision:double = 0.001;//0.000001; //min_modularity.   
    var type_file:int = 0; // 0: unweighted, 1: weighted
    var passTimes:int = -1;  // -1: detect until be stopped itself, else detect decided times. 
    var fileProfile:ProfileWriter;
    val now = Timer.milliTime(); //using for defining profile file name seperately 
    var showNodeMovePerPlace:boolean = false;
    var showModularity:boolean = false;
    var profile_d:boolean = false; //get detail profile per process
    var isRandom:boolean = true;
    var startMemory:long = 0l;

    @Native("c++", "GC_get_heap_size()")
    public static native def getGCMemSize():long;

    @Native("c++", "GC_INIT()")
    public static native def initGC():void;

    public  def checkArgs(args:Array[String])
    {
        for (i in args) {
            if (args(i).equals("-heap")) {
                //true: store or print the profile file
                heap = true;
            }  
            if (args(i).equals("-d")) {
                //true: store or print the profile file
                profile_d = true;
            }   
            if (args(i).equals("-p")) {
                //true: store or print the profile file
                profile = true;
                fileProfile = new ProfileWriter("Profile-"+Place.ALL_PLACES+"-"+now+".csv");
            }
            if (args(i).equals("-h")) {
                //true: store log file as csv type 
                //false: directly print out the detail result
                humanfriendlylog = true;
            } 
            if (args(i).equals("-i")) {
                filename = args(i+1);
            } 
            if (args(i).equals("-r")) {
                it_random = 0;
            }
            if (args(i).equals("-precision")) {
                precision = Double.parse(args(i+1));
            }
            if (args(i).equals("-w")) {
            //set file type "-w" means weightd file
                type_file = 1;
                filename_w = args(i+1);
            }
            if (args(i).equals("-pass")) {
            //set file type "-w" means weightd file
                passTimes = Int.parse(args(i+1));  
            }
            if (args(i).equals("-m")) {
            //show #(moved nodes) of each place
                showNodeMovePerPlace = true;
            }
            if (args(i).equals("-mod")) {
            //show #(moved nodes) of each place
                showModularity = true;
            }
            if (args(i).equals("-noRandom")) {
                isRandom = false;
            }
        }
    }

    public static def main(args:Array[String]) {   
        PlaceGroup.WORLD.broadcastFlat(()=>{
            try {
                val placeNum = Place.ALL_PLACES;
                val team = Team.WORLD;
                val role = here.id;
                var start:long = Timer.milliTime();
         
                var cd:ComDetect = new ComDetect(); 
                cd.checkArgs(args);
                if(cd.heap){
                    cd.initGC();
                    cd.startMemory = cd.getGCMemSize();   
                }
                
                //======================
                //read graph file 
                //Setp1: Read graph file 
                //======================
                var readStart:long = Timer.milliTime();
                var com:Community = new Community(cd.filename, cd.filename_w, 
                    cd.type_file, cd.passTimes, cd.precision);
                var readEnd:long = Timer.milliTime();

                var g:Graph = new Graph();
                var improvement:boolean = true;

                var mod:double = com.initModularity();
                
                if(role==0) Console.OUT.println("original modularity = " + mod + ", PlaceNum = " +placeNum);
                if(cd.profile && role==0) {
                    cd.fileProfile.writeln("original modularity = " + mod + ", PlaceNum = " +placeNum);
                    cd.fileProfile.writeln("#Place, #nodes, #links");
                    cd.fileProfile.writeln(Place.ALL_PLACES + ", " + (com.g.nb_nodes+1) + ", " + com.totalWeight);
                }
                
                var new_mod:double = 0.0; 
                var level:int = 0;
                var resetTotal:long = 0;
                var computeTotal:long = 0;
                var startCompute:long = 0l;
                var endCompute:long = 0l;
                var startReset:long = 0l;
                var endReset:long = 0l;
                do {
                    if(role == 0) {
                        Console.OUT.println("-------------------------------------------------------------------------------------------------------------");
                        Console.OUT.println("level: " + level );
                        Console.OUT.println("start computation");
                        Console.OUT.println("network size:" + (com.g.nb_nodes+1) + " nodes, " + com.g.nb_links + " links, " + com.g.total_weight + " weight.");
                    }
                    
                    //compute one level
                    startCompute = Timer.milliTime();

                    improvement = (level==0) ? com.one_level(level, cd.profile, cd.humanfriendlylog, mod, 
                            cd.now, cd.profile_d, cd.fileProfile, cd.heap, cd.showNodeMovePerPlace, 
                            cd.showModularity, cd.isRandom, cd.startMemory) : 
                        (role==0) ? com.one_level_seq(level, cd.isRandom, mod) : false;
                    
                    endCompute = Timer.milliTime();
                    if(role == 0) Console.OUT.println("It used "+(endCompute-startCompute)+"ms to Compute");
                    computeTotal += (endCompute-startCompute);
                    
                    startReset = Timer.milliTime();
                    
                    if(level==0) {
                        new_mod = com.mod;
                        var graphBuff:Array[GraphBuff] = new Array[GraphBuff](1);
                        graphBuff(0) = com.contractGraph();
                        var graphBuffA:Array[GraphBuff] = new Array[GraphBuff](placeNum);
                        team.barrier(role);
                        team.gather(role, 0, graphBuff, 0, graphBuffA, role, 1);
                        
                        if(role==0) com = new Community(graphBuffA, -1, cd.precision, com.size);
                    } else {
                        if(role==0) {
                            g = com.contractGraph_seq();
                            com = new Community(g, -1, cd.precision, com.inside_tmp);
                            new_mod = com.modularity();
                        }
                    }

                    level++;
                    if (level == 1) {
                        improvement = true;    
                    }

                    endReset = Timer.milliTime();
                    resetTotal += (endReset-startReset);
                    if(role == 0){
                        Console.OUT.println("Contract Time:  "+(endReset-startReset)+"ms");
                        Console.OUT.println("mod -> new_mod: " + mod + "->" + new_mod);
                    }
                    mod = new_mod;
                } while(improvement);
            
                var end:long = Timer.milliTime();
                if (role == 0) {
                    Console.OUT.println("Read: " + (readEnd - readStart) + "ms, place = " + role);
                    Console.OUT.println("Compute: " + computeTotal+ "ms, place = " + role);
                    Console.OUT.println("Reset: " + resetTotal + "ms, place = " + role);
                    Console.OUT.println("All: "+(end-start)+"ms");
                    Console.OUT.println("Modularity: " + new_mod);                
                } 
                if(cd.profile && role==0) {
                    cd.fileProfile.writeln("Read, Compute, Reset, All, Modularity");
                    cd.fileProfile.writeln((readEnd-readStart)+","+computeTotal+","+resetTotal+","+(end-start)+","+new_mod);
                    cd.fileProfile.close();
                }
           }catch(e:CheckedThrowable){  Console.OUT.println("error: " + e.toString());}
        });
    }
}
