import x10.util.Team;
import x10.util.ArrayList;
import x10.util.*;
import x10.lang.Exception;
import x10.lang.Place;
import x10.io.File;
import x10.io.Reader;
import x10.io.FileWriter;
import x10.io.FileReader;
import x10.io.BufferedReader;
import x10.compiler.*;

public final class DistributedGraphReader {

    private static val INT = 0;
    private static val DOUBLE = 1;
    private static val FLOAT = 2;
    private static val LONG = 3;

    var filename:String = null;
    var dataType:int = 0;
    var totalWeight:double;
    var nb_nodes:int;
    var nb_nodesP:int; //#nodsOfPlace
    
    var links:Array[int];
    var weights:Array[double];
    var degrees:Array[int];
    var originalDegrees:Array[int];

    var masterP:Array[int];
    var nodeList:Array[int];

    @Native("c++", "GC_get_heap_size()")
    public static native def getGCMemSize():long;

    @Native("c++", "GC_INIT()")
    public static native def initGC():void;

    static def length(dataType:int):Int {
        var length:int = 0;
        switch(dataType) {
            case INT:    
                length = 4;
            case DOUBLE: 
                length = 8;
            case LONG:   
                length = 8;
            case FLOAT:  
                length = 4;
        }
        return length;
    }

    def checkArgs( args:Array[String] ) {
        for (i in args) {
            if (args(i).equals("-i")) {
                filename = args(i+1);
            }  
            if (args(i).equals("-type")) {
                val dataTypeTmp = args(i+1);
                if(dataTypeTmp.equals("int")) {
                        dataType = INT;
                }
                if(dataTypeTmp.equals("double")) {
                        dataType = DOUBLE;
                }
                if(dataTypeTmp.equals("long")) {
                        dataType = LONG;
                }
                if(dataTypeTmp.equals("float")) {
                        dataType = FLOAT;
                }
            }   
        }
    }
 
    public def read(filename:String, dataType:int) {
        val team = Team.WORLD;
        val role = here.id;
        val placeNum = Place.ALL_PLACES;
        
        val file = new File(filename);
        val reader = new BufferedReader(new FileReader(file));
        
        nb_nodes = reader.readInt();
        nb_nodesP = reader.readInt();
        totalWeight = reader.readDouble();

        nodeList = new Array[int](nb_nodesP, 0);
        originalDegrees = new Array[int](nb_nodesP, 0);
        degrees = new Array[int](nb_nodesP+1, 0);
        masterP = new Array[int](nb_nodesP, 0);

        degrees(0) = 0;
        var deg:int = nb_nodesP - 1;
        for (node in 0..deg) {
            nodeList(node) = reader.readInt();
            originalDegrees(node) = reader.readInt();
            degrees(node+1) = reader.readInt();
            masterP(node) = reader.readInt();
        }

        val nb_links = degrees(nb_nodesP);
        links = new Array[int](nb_links, 0);
        weights = new Array[double](nb_links, 1.0);
        deg = nb_links - 1;
        for (linkIndex in 0..deg) {
            links(linkIndex) = reader.readInt();
        }
    }

    public def clear() {
        degrees = null;
        links = null;
        weights = null;
        originalDegrees = null;
        masterP = null;
        nodeList = null;
    }

    public static def main(args:Array[String]) {
        finish for (p in Place.places()) async at (p){
            val distFileReader = new DistributedGraphReader();
            val start = Timer.milliTime();
            distFileReader.read(args(0), 0);
            Console.OUT.println(Timer.milliTime()-start + "ms, place = " + here.id);
        }
    }
 }
