import x10.array.Array;

public class N2CPMessage {
	var nodesM:Array[int];
	var n2cM:Array[int];
	var deltaQM:Array[double];

	def this() {
		nodesM = new Array[int](1);
		n2cM = new Array[int](1);
		deltaQM = new Array[double](1);
	}

	public def this(nodes:Array[int], n2c:Array[int], deltaQ:Array[double]) {
		nodesM = nodes;
		n2cM = n2c;
		deltaQM = deltaQ;
	}
	
	public def setMessage(size:int) {
		nodesM = new Array[int](size);
		n2cM = new Array[int](size);
		deltaQM = new Array[double](size);
	}

	public def setMessage(nodes:Array[int], n2c:Array[int]) {
		nodesM = nodes;
		n2cM = n2c;
	}
}
