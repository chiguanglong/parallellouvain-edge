import x10.array.Array;
import x10.util.HashSet;
import x10.lang.Place;
import x10.lang.Iterator;
import x10.util.ArrayList;

public class CommunityBuff {
    var borders:Array[Array[int]] = new Array[Array[int]](1);
    var bordersH:Array[ArrayList[int]] = new Array[ArrayList[int]](1);
    var borderSize:Array[int] = new Array[int](1);
    var indexes:Array[int] = new Array[int](1);

    var nodesSrc:Array[int] = new Array[int](1);
    var n2cPSrc:Array[Array[int]] = new Array[Array[int]](1);
    var totPSrc:Array[Array[double]] = new Array[Array[double]](1);
    var insidePSrc:Array[Array[double]] = new Array[Array[double]](1);

    def this() {
        borders(0) = new Array[int](1);
        bordersH(0) = new ArrayList[int](1);
        borderSize = new Array[int](1);

        n2cPSrc(0) = new Array[int](1);
        totPSrc(0) = new Array[double](1);
        insidePSrc(0) = new Array[double](1);
    }

    def setCommBuff(size:int) {
        n2cPSrc = new Array[Array[int]](size);
        totPSrc = new Array[Array[double]](size);
        insidePSrc = new Array[Array[double]](size);
        indexes = new Array[int](size, 0);
    }

    def setBorderBuff(bordersP:Array[ArrayList[int]], deltaQ:Array[double], n2cP:Array[int]){
        type MSG = N2CPMessage;
        val messages = new Array[MSG](bordersP.size);
        n2cPSrc = new Array[Array[int]](bordersP.size);

        for (place in bordersP) {
            val borderNodeSize = bordersP(place).size();
            n2cPSrc(place) = new Array[int](borderNodeSize);
            nodesSrc = new Array[int](borderNodeSize);

            var count:int = 0;
            val it = bordersP(place).iterator();
            val deltaQPSrc = new Array[double](borderNodeSize);
            
            while(it.hasNext()) {
                val borderNode = it.next();
                nodesSrc(count) = borderNode;
                deltaQPSrc(count) = deltaQ(borderNode);
                n2cPSrc(place)(count) = n2cP(borderNode);
                count++;
            }
            messages(place) = new N2CPMessage(nodesSrc, n2cPSrc(place), deltaQPSrc);
        }
        n2cPSrc = null;
        nodesSrc = null;
        return messages;
    } 

    def clearCommBuff() {
        n2cPSrc = null;
        totPSrc = null;
        insidePSrc = null;
    }   
}
