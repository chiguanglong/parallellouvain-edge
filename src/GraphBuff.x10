import x10.array.Array;
import x10.util.HashSet;
import x10.lang.Place;
import x10.lang.Iterator;
import x10.util.ArrayList;

public class GraphBuff {
    var degrees:Array[int];
    var links:Array[int];
    var weights:Array[double];

    var indexes:Array[int];
    var commSize:int;

    def this(degreesP:Array[int], linksP:Array[int], weightsP:Array[double], 
        indexesP:Array[int], commSizeP:int) {
        degrees = degreesP;
        links = linksP;
        weights = weightsP;
        indexes = indexesP;
        commSize = commSizeP;
    }

    def clear() {
        degrees = null;
        links = null;
        weights = null;
        indexes = null;
    }
}
