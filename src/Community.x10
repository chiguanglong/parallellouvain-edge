import x10.util.*;
import x10.util.ArrayList;
import x10.compiler.*;
import x10.lang.*;
import x10.lang.Place;
import org.scalegraph.util.tuple.*;
import x10.array.PlaceGroup;
import x10.array.Region;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;

public class Community {
	var g:Graph;
	var size:int;

	//being used to aggregate neighbor community info
	//and neigh_pos also be used to store <commID, newGraphNodeID> info when contracting graph data
	var neigh_weight:Array[double];
	var neigh_pos:Array[int];
	var neigh_last:int;
	
	var n2c:Array[int];
	var tot:Array[double];
	var inside:Array[double];
	var inside_tmp:ArrayList[double];
	var deltaQ:Array[double];
	
	var needRenumber:boolean;

	var nb_pass:int;
	var min_modularity:double;
	var totalWeight:double;
	var modP:double; //modularity of each place
	var mod:double;  //total modularity

	//the node range [start_p, end_p) will be assigned to each place
	//band = #Nodes / #Place
	var team:Team;
	var startP:int;
	var endP:int;
	var band:int;
	var nodeBand:int;
	var role:int;
	var placeNum:int;

	var commBuffP:CommunityBuff;

	public def this(){ 
		g = new Graph();
	}

	public def this(filename:String, 
		filename_w:String, //filename_w will specify weighted file
		type_file:int, 	// 0: unweighted, 1: weighted
		passTimes:int, 	// -1: detect until be stopped itself, else detect decided times. 
		precision:double) { //0.000001; //min_modularity.	

		//read graph separately and init community info 
		team = Team.WORLD;
		role = here.id;
		placeNum = Place.ALL_PLACES;
		needRenumber = false;

		g = new Graph(filename +"-"+role+".bin", filename_w, type_file);	

		size = g.nb_nodes;
        band = (size+1) / placeNum;

		neigh_weight = new Array[double](g.nb_nodesP,-1.0);
		neigh_pos = new Array[int](g.nb_nodesP);
		neigh_last = 0;

		n2c = new Array[int](size+1, (i :Int) => i);
		tot = new Array[double](size+1);
		inside = new Array[double](size+1);
		deltaQ = new Array[double](size+1);

		//set replica size for each place
		commBuffP = new CommunityBuff();
		commBuffP.bordersH = new Array[ArrayList[int]](placeNum);
		for(i in commBuffP.bordersH) {
			commBuffP.bordersH(i) = new ArrayList[int]();
		}

		//set node's n2c, total weight, inside weight
        for(index in g.nodeList) {
            val node = g.nodeList(index);
			n2c(node) = node;
			tot(node) = g.originalDegrees(index);
			deltaQ(node) = 0.;
			inside(node) = 0.;

			val place = g.masterP(index);
			if(place!=role) { 
				commBuffP.bordersH(place).add(node);
			}
		}

		nb_pass = passTimes;
		min_modularity = precision;
		totalWeight = g.total_weight;

		initBuff();
	}

	public def this(graphBuffA:Array[GraphBuff], passTimes:int, precision:double, newGraphSize:int) {  
	//using when reset graph, precision == minm(in c++ version)
		size = newGraphSize;
		neigh_weight = new Array[double](size, -1.0);
		neigh_pos = new Array[int](size-1);
		neigh_last = 0;

		n2c = new Array[int](size, 0);
		inside = new Array[double](size, 0);
		tot = new Array[double](size, 0);

		g = new Graph();
		g.nb_nodes = size - 1;

		g.degrees = new Array[int](size+1, 0);
		g.degrees(0) = 0;
		
		val links_tmp = new Array[ArrayList[int]](size);
		val weights_tmp = new Array[ArrayList[double]](size);

		for (i in links_tmp) {
			links_tmp(i) = new ArrayList[int]();
			weights_tmp(i) = new ArrayList[double]();
		}

		var start:int = 0;
		for (graphId in graphBuffA) {
			val graph = graphBuffA(graphId);
			val deg = graphBuffA(graphId).commSize;
			start = 0;
			for (i in 0..deg) {
				val node = graph.indexes(i);

				start += (i==0) ? 0 : graph.degrees(i-1);
				val end = graph.degrees(i) + start;
				for (var j:int=start; j<end; j++) {
					val neigh = graph.links(j);
					val neigh_w = graph.weights(j);
					if(node==neigh) inside(node) += neigh_w;
					links_tmp(node).add(neigh);
					weights_tmp(node).add(neigh_w);
				}
			}
		}

		val neighList = new HashSet[int](); //in order to compute nb_links
		var nb_links:int = 0;
		val deg = size - 1;

		for (node in 0..deg) {
			neighList.clear();
			val linkSize = links_tmp(node).size()-1;
			for (linkNode in 0..linkSize) {
				neighList.add(links_tmp(node)(linkNode));
			}
			nb_links += neighList.size();
		}

		g.nb_links = nb_links;
		g.links = new Array[int](nb_links);
		g.weights = new Array[double](nb_links);

		val linkNodePos = new HashMap[int, int]();
		linkNodePos.clear();
		var linkIndex:int = 0;
		for (node in 0..deg) {
			val linkSize = links_tmp(node).size()-1;
			for (linkNode in 0..linkSize) {
				val neigh = links_tmp(node)(linkNode);
				val neigh_w = weights_tmp(node)(linkNode);
				if(!linkNodePos.containsKey(neigh)) {
					g.links(linkIndex) = neigh;
					g.weights(linkIndex) = neigh_w;
					linkNodePos.put(neigh, linkIndex);
					linkIndex++;
				} else {
					var bIndex:Box[int] = linkNodePos.get(neigh);
					val index = bIndex.value;
					g.weights(index) += neigh_w;
				}
			}
			g.degrees(node+1) = linkNodePos.size() + g.degrees(node);
			linkNodePos.clear();
		}	


		neigh_weight = new Array[double](size,-1.0);
		neigh_pos = new Array[int](size-1);
		neigh_last = 0;

		g.total_weight = 0.0;
		for (node in (0..deg)) {
			n2c(node) = node;
			tot(node) = g.weighted_degree(node);
			g.total_weight += tot(node);
		}
		nb_pass = passTimes;
		min_modularity = precision;
		totalWeight = g.total_weight;
		g.nb_links = g.degrees(size);
		size = size -1;

		Console.OUT.println("totalweight = " + totalWeight);
	}

	public def this(gc:Graph, passTimes:int, precision:double, inside_tmp:ArrayList[double]) {  
	//being used in sequential mod
		g = gc;
		size = g.nb_nodes - 1;

		neigh_weight = new Array[double](size+1,-1.0);
		neigh_pos = new Array[int](size);
		neigh_last = 0;

		n2c = new Array[int](size+1);
		inside = new Array[double](size+1);
		tot = new Array[double](size+1);
		for (index in (0..size)) {
			n2c(index) = index;
			tot(index) = g.weighted_degree(index);
			inside(index) = inside_tmp(index)/2;
		}

		nb_pass = passTimes;
		min_modularity = precision;
		totalWeight = g.total_weight;
	}

    @Native("c++", "GC_get_heap_size()")
	public static native def getGCMemSize():long;

	@Native("c++", "GC_INIT()")
	public static native def initGC():void;

	@NonEscaping final @Inline public def modularity() {
		var q:double = 0.0;
		var totSum:double = 0.;
		var inSum:double = 0.;

		for (i in tot) {
			if(tot(i)>0) {
				q += inside(i)/totalWeight - (tot(i)/totalWeight)*(tot(i)/totalWeight);
				totSum += tot(i);
				inSum += inside(i);
			}	
		}
		return q;
	}

	public def initModularity() {
		//compute initial modularity of the graph data
		Console.OUT.println("totalweight = " + totalWeight);
		
		modP = 0.;
		for (index in g.nodeList) {
			if(g.masterP(index)==role) {
				val node = g.nodeList(index);
				modP += inside(node)/totalWeight - (tot(node)/totalWeight)*(tot(node)/totalWeight);
			}
		}
		team.barrier(role);
        mod = team.allreduce(here.id, modP, Team.ADD);
		return mod;
	}

	@Inline public def neigh_comm(node:int) {
		//being used in sequential mode
		var startIndex:int = 0;
		var deg:int = neigh_last-1;
		var region:Region = Region.make(0, deg);
		for ([i] in region) {
			neigh_weight(neigh_pos(i)) = -1.0;
		}
		neigh_last = 0;
		neigh_pos(0)=n2c(node);
		neigh_weight(neigh_pos(0)) = 0;
		neigh_last = 1;
		
        startIndex = g.degrees(node);
        deg = g.degrees(node+1)-1;

        var neigh:int = 0;
		var neigh_comm:int = 0;
		var neigh_w:double = 0.0;
		region = Region.make(startIndex, deg);
	    for (i in region){
			neigh = g.links(i);
			neigh_comm = n2c(neigh);
			neigh_w = g.weights(i);
			if (neigh!=node) {
				if (neigh_weight(neigh_comm)==-1.0) {
					neigh_weight(neigh_comm) = 0.0;
					neigh_pos(neigh_last++) = neigh_comm;
				}
				neigh_weight(neigh_comm) += neigh_w;
			}	
	    }
	}

	@Inline public def remove( node:int, comm:int, dnodecomm:double){
		//being used in sequential mode
		//AssertionError("Node`s ID not correct!!",(node < size));
		tot(comm) -= g.weighted_degree(node);
		inside(comm) -= 2*dnodecomm + g.nb_selfloops(node);
		n2c(node) = -1;
	}

	@Inline public def insert( node:int, comm:int, dnodecomm:double) {
		//being used in sequential mode
		//AssertionError("Node`s ID not correct!!",(node < size));
		tot(comm) += g.weighted_degree(node);
		inside(comm) += 2*dnodecomm + g.nb_selfloops(node);
		n2c(node) = comm;
	}

	@Inline public def neigh_comm( node:int, index:int, pass:int) {	
		var startIndex:int = 0;
		var deg:int = neigh_last-1;
		var region:Region = Region.make(0,deg);
		for ([i] in region) {
			neigh_weight(neigh_pos(i)) = -1.0;
		}

		neigh_last = 0;
		neigh_pos(0)=n2c(node);

		neigh_weight(neigh_pos(0)) = 0;
		neigh_last = 1;
		
	    startIndex = g.degrees(index) - g.degrees(0);
	    deg = g.degrees(index+1) - g.degrees(0) - 1;

		var neigh:int = 0;
		var neigh_comm:int = 0;
		var neigh_w:double = 0.0;
		region = Region.make(startIndex,deg);
	    for ([i] in region) {
			neigh = 0;
			neigh_comm = 0;
			neigh_w = 0.0;
			
			neigh = g.links(i);
			neigh_comm = n2c(neigh);	
			neigh_w = g.weights(i);

			if (node!=neigh) {		
				if (neigh_weight(neigh_comm)==-1.0) {
					neigh_weight(neigh_comm) = 0.0;
					neigh_pos(neigh_last++) = neigh_comm;
				}
				neigh_weight(neigh_comm) += neigh_w;
			}
	    }
	}

	@Inline public def remove(index:int, node:int, comm:int, dnodecomm:double, w_degree:double) {
		var totTmp:double = tot(comm);
		var inTmp:double = inside(comm);
		totTmp -= w_degree;
		inTmp -= 2*dnodecomm + 0.0;//g.nb_selfloops(newNode, nodeBand);

		tot(comm) = totTmp;
		inside(comm) = inTmp;
		n2c(node) = -1;
	}

	@Inline public def insert(index:int, node:int, comm:int, dnodecomm:double, w_degree:double) {
		var totTmp:double = tot(comm);
		var inTmp:double =  inside(comm);

		totTmp += w_degree;
		inTmp += 2*dnodecomm + 0.;//g.nb_selfloops(newNode, nodeBand);
		
		tot(comm) = totTmp;
		inside(comm) = inTmp;
		n2c(node) = comm;
	}

	@Inline public def modularity_gain(totc:double, dnodecomm:double, w_degree:double) {
		return (dnodecomm - totc*w_degree/totalWeight);
	}

	public def randomizeGraphNodes(randomSize:int) {
		var random_order:Array[int] = new Array[int](randomSize+1, (i:int)=> i);
		var r:Random = new Random();
		for (i in (0..randomSize)) {
			val rand_pos = r.nextInt(randomSize+1);
			val tmp = random_order(i);
			random_order(i) = random_order(rand_pos);
			random_order(rand_pos) =tmp;
		}
		return random_order;
	}

	public def one_level_seq(level:int, isRandom:boolean, modularity:double) {

		Console.OUT.println("Compute in Sequential mode!");

		var improvement:boolean = false;
		var nb_moves:int = 0;
		var nb_pass_done:int = 0;

		var new_mod:double = modularity;
		var cur_mod:double = new_mod;

		var best_comm:int;
		var best_nblinks:double = 0.0;
		var best_increase:double = 0.0;

		val random_order = randomizeGraphNodes(size);

		do { 
			cur_mod = new_mod;
			nb_moves = 0;
			nb_pass_done++;

			var node:int = 0;

			for (node_tmp in (0..size)) {
				node = (isRandom) ? random_order(node_tmp) : node_tmp;
				val node_comm = n2c(node);
				val w_degree = g.weighted_degree(node);
				
				neigh_comm(node);

				remove(node, node_comm, neigh_weight(node_comm));

 				best_comm = node_comm;
 				best_nblinks = neigh_weight(node_comm);
 				best_increase = modularity_gain(tot(best_comm), best_nblinks, w_degree);

 				for (var i:int=1; i<neigh_last; i++) {
 					val increase = modularity_gain(tot(neigh_pos(i)), neigh_weight(neigh_pos(i)), w_degree);
 					if ((increase > best_increase)) {
 						best_comm = neigh_pos(i);
 						best_nblinks = neigh_weight(neigh_pos(i));
 						best_increase = increase;
 					}	
 				}

 				insert(node, best_comm, best_nblinks);

 				if (best_comm!=node_comm) {
 					nb_moves++;
 				}
			}

			new_mod = modularity();

			if (nb_moves>0) {
				improvement = true;
			}
		} while (nb_moves>0 && (new_mod-cur_mod)>min_modularity);
		return improvement;
	}

	public def one_level_parallel(pass:int, isRandom:boolean) {	
    	var nb_moves:int = 0;

		var best_comm:int;
		var best_nblinks:double = 0.0;
		var best_increase:double = 0.0;
    	
    	val random_order = randomizeGraphNodes(g.nodeList.size-1);

    	var startTime:long = Timer.milliTime();
    	var w_degree:double = 0.;

    	deltaQ.clear();

    	val nodeSize = g.nodeList.size - 1;
    	for (index in 0..nodeSize) {
    		val newIndex = isRandom ? random_order(index) : index;
    		val node = g.nodeList(newIndex);
    		val node_comm = n2c(node);
    		w_degree = g.originalDegrees(newIndex);
 			
    		neigh_comm(node, newIndex, pass);
    		remove(newIndex, node, node_comm, neigh_weight(node_comm), w_degree);

    		best_comm = node_comm;
 			best_nblinks = neigh_weight(node_comm);
 			best_increase = modularity_gain(tot(best_comm), best_nblinks, w_degree);

    		val deg = neigh_last -1;
    		for (i in (1..deg)) {
	 			val neigh_comm_tmp = neigh_pos(i);
				val increase = modularity_gain(tot(neigh_comm_tmp), neigh_weight(neigh_comm_tmp), w_degree);
				if ((increase > best_increase)) {
					best_comm = neigh_comm_tmp;
					best_nblinks = neigh_weight(neigh_comm_tmp);
					best_increase = increase;
				}
 			}
 			insert(newIndex, node, best_comm, best_nblinks, w_degree);
 			deltaQ(node) = best_increase;
 			if (best_comm!=node_comm) {
 				nb_moves++;
 			}
    	}

    	return nb_moves;
	}

	public def one_level(level:int, profile:boolean, humanfriendlylog:boolean, 
		modularity:double, now:long, profile_d:boolean, fileProfile:ProfileWriter,
		heapB:boolean, showMove:boolean, showMod:boolean, isRandom:boolean, startMemory:long) {	
		
		if(role==0) Console.OUT.println("Level " + level + ", "+ "Parallel mode!");
		
		val levelStartTime = Timer.milliTime();
		//define for outputing profile 
		var fileProfWriter:ProfileWriter = null;
		var heapProfWriter:ProfileWriter = null;
		
		if(profile_d) {
    		fileProfWriter = new ProfileWriter("Profile-" + placeNum + "-" + role + "-" + now + ".csv");
			fileProfWriter.writeln("level,pass,place,#node,#links,computeTimeP(ms),reduceMoves(ms),aggregate(ms), reduceMod, moves, modP");
		}
		if(humanfriendlylog) {
			Console.OUT.println("level, pass, place, #nodes, #links, " + 
				"computeTime(ms), reduceMoveTime(ms), aggregateTime(ms), reduceModTime(ms), moves, modP");
		}
		
		if(heapB) {
    		 		heapProfWriter = new ProfileWriter("HeapProfile-" + placeNum + "-" + role + "-" + now + ".csv");
    		heapProfWriter.writeln("startMemory, " + startMemory + ", init, " + getGCMemSize());
			heapProfWriter.writeln("level, pass, place, computeStart, computeEnd, aggregateStart, aggregateEnd");
		}

		var nb_pass_done:int = 0;
		var improvement:boolean = false;
		var nb_moves:int = 0;

		var new_mod:double = modularity;
		var cur_mod:double = new_mod;

		var totalCommunicateTime:long = 0l;
		var totalComputeTime:long = 0l;
		var totalAggregateTime:long = 0l;
		var totalMoves:int = 0;

		do {
			//record time
			val computeStartTime = Timer.milliTime();
			
			cur_mod = new_mod;
			nb_moves = 0;
			nb_pass_done++;

			if(heapB) {
				heapProfWriter.write(level + ", " + 
					nb_pass_done + ", " + role + ", " + getGCMemSize() + ", ");
			}

			// Phase 1: For each node, find the max ¥Delta Q among the neighbor, where the node is to be moved
			// Computation only
			val moves = one_level_parallel(nb_pass_done, isRandom);

			if(heapB) {
				heapProfWriter.write(getGCMemSize() + ", ");
			}

			//record time
			val computeEndTime = Timer.milliTime();
			totalComputeTime += (computeEndTime - computeStartTime);

			//Phase 2: aggregate total moves by allreduce operation
			team.barrier(role);
			nb_moves = team.allreduce(role, moves, Team.ADD);

			//record time
			val reduceMoveEndTime = Timer.milliTime();
	    	
	    	if(heapB) {
				heapProfWriter.write(getGCMemSize() + ",");
			}
			//Phase 3: aggregate n2c, tot, in info for next round computing 
			aggregateTotIn();

			if(heapB) {
				heapProfWriter.writeln(getGCMemSize() + ",");
			}

			//record time
			val aggregateEndTime = Timer.milliTime();
			totalAggregateTime += (aggregateEndTime-reduceMoveEndTime);

			//Phase 4: aggregate total modularity by allreduce operation
			new_mod = mod;//team.allreduce(role, modP, Team.ADD);
			//record time
			val reduceModEndTime = Timer.milliTime();
			totalCommunicateTime += (reduceModEndTime-computeEndTime);

			if (nb_moves>0) {
	        	improvement = true;
	        }
	        totalMoves +=nb_moves;

			if(profile_d) {
				fileProfWriter.writeln(level + "," + nb_pass_done + "," + role + "," + g.degrees.size + "," + g.links.size +","
					+ (computeEndTime-computeStartTime) + "," + (reduceMoveEndTime-computeEndTime) +"," 
					+ (aggregateEndTime-reduceMoveEndTime) + ", " + (reduceModEndTime-aggregateEndTime) +"," + moves + ", " + modP);	
				fileProfWriter.flush();
			} 
			
			if(humanfriendlylog) {
				Console.OUT.println(level + "," + nb_pass_done + "," + role + "," + g.degrees.size + "," + g.links.size +","
					+ (computeEndTime-computeStartTime) + "," + (reduceMoveEndTime-computeEndTime) +"," 
					+ (aggregateEndTime-reduceMoveEndTime) + ", " + (reduceModEndTime-aggregateEndTime) +"," + moves + ", " + modP);		
			}

			if(showMove)
				Console.OUT.println("Place = " + role + ", pass = " + nb_pass_done + ", moves = " + moves);
			if(showMod) { 
				Console.OUT.println("level: " + level +", pass: " + nb_pass_done + ", mod = " + modP + ", place = " + role); 
			}
			if(profile)	{
				fileProfile.writeln(level + ", " + nb_pass_done + 
					", " + mod + ", " + nb_moves + ", " + here.id);
			}
		} while(nb_moves>0 && (new_mod-cur_mod)>min_modularity);
		
		val levelEndTime = Timer.milliTime();
		if (role == 0) Console.OUT.println("pass = " + nb_pass_done + ", total moves = " + totalMoves + ", modularity = " + new_mod);
		if(profile){
			fileProfile.writeln("level, pass, totalComputeTime(ms), totalAggregateTime(ms), totalCommunicateTime(ms), levelTime(ms), totalMoves, modularity");
			fileProfile.writeln(level + ", " + nb_pass_done +", " + totalComputeTime + ", " 
				+ totalAggregateTime +", " + totalCommunicateTime +", "+(levelEndTime-levelStartTime)+", " 
				+ nb_moves + ", " + new_mod);
		}
		if(humanfriendlylog){
			Console.OUT.println("level, pass, totalComputeTime(ms), totalAggregateTime(ms), totalCommunicateTime(ms), levelTime(ms), totalMoves, modularity");
			Console.OUT.println(level + ", " + nb_pass_done +", " + totalComputeTime + ", " 
				+ totalAggregateTime +", " + totalCommunicateTime +", "+(levelEndTime-levelStartTime)+", " 
				+ nb_moves + ", " + new_mod);
		}

		return improvement;
	}

	public def aggregateTotIn() {
		//aggregate n2c, tot, in infomation
   		//msg consist of (borderNode/replica, deltaQ, comm)
   		var borderMsgSrc:Array[N2CPMessage] = commBuffP.setBorderBuff(commBuffP.bordersH, deltaQ, n2c);
   		var borderMsgDst:Array[N2CPMessage] = new Array[N2CPMessage](placeNum);
   		
   		team.barrier(role);

   		finish for (i in (1..placeNum)) {
			team.scatter(role, i-1, borderMsgSrc, i-1, borderMsgDst, i-1, 1);
   		}

   		//set local main/master nodes' community ID
   		//return updated replicas' n2c info
        borderMsgSrc = SetN2C_local(borderMsgDst);

      	team.barrier(role);

   		finish for (i in (1..placeNum)) {
			team.scatter(role, i-1, borderMsgSrc, i-1, borderMsgDst, i-1, 1);
   		}     

   		//set local borders/replicas community ID
        SetN2C_border_local(borderMsgDst);

        borderMsgSrc = null;
        borderMsgDst = null;

        //compute local tot, in info seperately and make commMsg using updated tot, in
        var commMsgSrc:Array[CommunityMessage] = ComputeTotIn_p();
   		var commMsgDst:Array[CommunityMessage] = new Array[CommunityMessage](placeNum);

        team.barrier(role);

        //send updated local tot, in info to specific remote computing node
		finish for(i in (1..placeNum)) {
        	team.gather( role, i-1, commMsgSrc, i-1, commMsgDst, i-1, 1);
        }

        //aggregate tot, in info in specific computing node
        //compute modularity of local main nodes' 
        commMsgSrc = UpdateTotIn(commMsgDst);
        commMsgDst = new Array[CommunityMessage](placeNum);

        //recieve and update updated tot, in info of main and border node
        team.barrier(role);

    	finish for(i in (1..placeNum)) {
        	team.gather( role, i-1, commMsgSrc, i-1, commMsgDst, i-1, 1);
        }
        //set local tot, in with the updated tot, in info
        SetTotin_local(commMsgDst);

        commBuffP.clearCommBuff();
        commMsgSrc = null;
        commMsgDst = null;
	}

	@NonEscaping final def initBuff() {
		val endIndex = placeNum - 1;
		commBuffP.setCommBuff(placeNum);
		for(i in (0..endIndex)) {
			val size = commBuffP.bordersH(i).size();
			commBuffP.n2cPSrc(i) = new Array[int](size);
	        commBuffP.totPSrc(i) = new Array[double](size);
	        commBuffP.insidePSrc(i) = new Array[double](size);	        
		}
	}

	public def UpdateTotIn(commMsgDst:Array[CommunityMessage]) {
		//aggregate tot, in in each specific computing node
        //compute modularity of local main nodes'
		var commList:HashSet[int] = new HashSet[int]();
		tot.clear();
		inside.clear();
		for (place in commMsgDst) {
			val msg = commMsgDst(place);
			val deg = msg.sizeM-1;
			for (count in (0..deg)) {
				val commTmp = msg.n2cM(count);
				tot(commTmp) += msg.totM(count);
				inside(commTmp) += msg.insideM(count);
			}
		}

		for (place in commMsgDst) {
			val msg = commMsgDst(place);
			val deg = msg.sizeM-1;
			for (count in (0..deg)) {
				val commTmp = msg.n2cM(count);
				commList.add(commTmp);
				msg.totM(count) = tot(commTmp);
				msg.insideM(count) = inside(commTmp);
			}
		}		

		var modTmp:double = 0.0;
		var it:Iterator[int] = commList.iterator();
		while (it.hasNext()) {
			val comm = it.next();
			modTmp +=  inside(comm)/totalWeight - (tot(comm)/totalWeight)*(tot(comm)/totalWeight);
		}

		modP = modTmp; // set modularity of each place
		var preMod:double = mod;

		team.barrier(role);
		mod = team.allreduce(role, modP, Team.ADD);

		if((mod-preMod)<min_modularity){
			needRenumber = true;
			//set new graph node IDs of current communities
			val commSize:int = commList.size();
			neigh_pos.clear();
			neigh_pos(0) = commSize;

			team.barrier(role);	

			//aggregate commSize to decide  contracted graph data's degree size
			size = team.reduce(role, 0, commSize, Team.ADD);

			for (i in 1..placeNum) {
				team.gather(role, i-1, neigh_pos, 0, neigh_pos, 1, 1);
			}

			var start:int = 0;
			for (var i:int=0; i<placeNum+1; i++) {
				if(i<role) start += neigh_pos(i+1);
			}

			//assign new noed id to the gommunity
			neigh_pos.clear();
			it = commList.iterator();
			var index:int = 0;

			while(it.hasNext()) {
				val comm = it.next();
				neigh_pos(comm) = (index + start);
				index++;
			}

			for (place in commMsgDst) {
				val msg = commMsgDst(place);
				val deg = msg.sizeM-1;
				for (count in (0..deg)) {
					val commTmp = msg.n2cM(count);
					msg.commNewNumM(count) = neigh_pos(commTmp);
				}
			}

			neigh_pos.clear();
		}

		commList = null;
		return commMsgDst;	
	}

	public def ComputeTotIn_p() {
	//compute total weight, inside weight of community locally only using local main/master nodes' n2c
	//and updated borders' n2c info
    	var tot_tmp:double = 0.0;
    	var in_tmp:double = 0.0;
    	var deg:int = endP -1;

		var neigh:int = 0;
		var neighComm:int = 0;

		//prevent from community totalweight being added duplicately
		var commList:HashSet[int] = new HashSet[int](band/2);

		tot.clear();
		inside.clear();

		for (index in g.nodeList) {
			val node = g.nodeList(index);
			val comm = n2c(node);
			commList.add(comm);
		
			val start = g.degrees(index);			
			val end = g.degrees(index+1)-1;	

			for (linkIndex in (start..end)) {
				neigh = g.links(linkIndex);
				neighComm = n2c(neigh);

				tot_tmp += g.weights(linkIndex);
				if(comm==neighComm){
					in_tmp += g.weights(linkIndex);
				}
			}
			tot(comm) = tot(comm) + tot_tmp;
			inside(comm) = inside(comm) + in_tmp;				
			
			tot_tmp = 0.0;
			in_tmp = 0.0;
		}

		var buffSize:Array[int] = new Array[int](placeNum, 0);
		var it:Iterator[int] = commList.iterator();
		while(it.hasNext()) {
			val comm = it.next();
			var place:int = comm / band;
			if( place >= placeNum ) place = placeNum -1;
			buffSize(place) += 1;
		}

		it = commList.iterator();
		commBuffP.setCommBuff(placeNum);
		while(it.hasNext()){
			val comm = it.next();
			var place:int = comm / band;
			if( place >= placeNum ) place = placeNum -1;

			if(commBuffP.indexes(place)==0){
				val sizeOfBuffer = buffSize(place);
				commBuffP.n2cPSrc(place) = new Array[int](sizeOfBuffer, 0);
				commBuffP.totPSrc(place) = new Array[double](sizeOfBuffer, 0.);
				commBuffP.insidePSrc(place) = new Array[double](sizeOfBuffer, 0.);
			}

			var count:int = commBuffP.indexes(place);
			commBuffP.n2cPSrc(place)(count) = comm;
			commBuffP.totPSrc(place)(count) = tot(comm);
			commBuffP.insidePSrc(place)(count) = inside(comm);
			commBuffP.indexes(place) += 1;
		}
		commList = null;

		val msg = new Array[CommunityMessage](placeNum);
		for (place in msg) {
			msg(place) = new CommunityMessage(commBuffP.n2cPSrc(place), commBuffP.totPSrc(place), 
				commBuffP.insidePSrc(place), commBuffP.indexes(place), new Array[int](band));
		}

		commBuffP.clearCommBuff();
		return msg;
	}

	public def SetN2C_local(message:Array[N2CPMessage]){
		//message consists of nodes(Array[int]), n2c(Array[int]), deltaQ(Array[double])
		for (i in message) {
			val msg = message(i);
			val size = msg.nodesM.size - 1;
			for (msgIndex in (0..size)) {
				val node = msg.nodesM(msgIndex);
				val comm = msg.n2cM(msgIndex);
				val deltaQP = msg.deltaQM(msgIndex);
				if((deltaQ(node) - deltaQP) < 0.) {
					n2c(node) = comm;
					deltaQ(node) = deltaQP;
				}
			}
		}

		for (i in message) {
			val msg = message(i);
			val size = msg.nodesM.size - 1;
			for (msgIndex in (0..size)) {
				val node = msg.nodesM(msgIndex);
				msg.n2cM(msgIndex) = n2c(node);
			}
		}
		return message;
	}

	public def SetN2C_border_local(message:Array[N2CPMessage]) {
		//message consists of nodes(Array[int]), n2c(Array[int]), deltaQ(Array[double])
		for (i in message) {
			val msg = message(i);
			val size = msg.nodesM.size - 1;
			for (msgIndex in (0..size)) {
				val node = msg.nodesM(msgIndex);
				val comm = msg.n2cM(msgIndex);
				n2c(node) = comm;
			}
		}
	}

	@NonEscaping final def SetTotin_local(commMsgUpdatedDst:Array[CommunityMessage]){
		for (place in commMsgUpdatedDst) {
			val msg = commMsgUpdatedDst(place);
			val deg = msg.sizeM - 1;
			for(count in (0..deg)) {
				val comm = msg.n2cM(count);
				tot(comm) = msg.totM(count);
				inside(comm) = msg.insideM(count);
				if(needRenumber) {
					neigh_pos(comm) = msg.commNewNumM(count);
				}
			}
		}
	}

	public def contractGraph_seq() {
	//sequential graph data contraction
		Console.OUT.println("Contraction in sequential mode!!");
		var startIndex:int = 0;
		var deg:int = 0;
		var neigh:int = 0;
		var neigh_comm:int = 0;
		var neigh_w:double = 0.0;
		var renumber:Array[int] = new Array[int](size+1, -1);
		var finalCount:int = 0;

		for (node in (0..size)) {
			renumber(n2c(node)) = 0;
		}

		for (i in (0..size)) {
			if (renumber(i)!=-1) {
				renumber(i)=finalCount;
				finalCount++;
			}
		}

		//Compute communities
		var comm_nodes:Array[ArrayList[int]] = new Array[ArrayList[int]](finalCount);
		inside_tmp = new ArrayList[double](finalCount);

		finalCount = finalCount - 1;
		for (i in (0..finalCount)) {
			comm_nodes(i) = new ArrayList[int]();
			inside_tmp(i) = 0.0;
		}

		for (node in (0..size)) {
			comm_nodes(renumber(n2c(node))).add(node);
			inside_tmp(renumber(n2c(node))) += inside(node);
		}
		
		var g2:Graph = new Graph();
		g2.nb_nodes = comm_nodes.size;
		g2.degrees = new Array[int](comm_nodes.size+1);
		g2.degrees(0) = 0;

		var comm_deg:int = comm_nodes.size-1;
		var set:Set[int];
		var it:Iterator[int];
		var links_tmp:ArrayList[int] = new ArrayList[int]();
		var weights_tmp:ArrayList[double] = new ArrayList[double]();
		for (comm in (0..comm_deg)) {
			var m:HashMap[int, double] = new HashMap[int, double]();
			var comm_size:int = comm_nodes(comm).size()-1;
			for ( node in (0..comm_size)) {
		        startIndex = g.degrees(comm_nodes(comm)(node));
		        deg = g.degrees(comm_nodes(comm)(node)+1)-1;

		        for (i in (startIndex..deg)) {
					neigh = g.links(i);
					neigh_comm = renumber(n2c(neigh));
					neigh_w = g.weights(i);

					if (comm==neigh_comm) {
						val inside_w:double = g.weights(i);
						inside_tmp(comm) = inside_tmp(comm) + inside_w;
					}		
					if(!m.containsKey(neigh_comm)) {
						m.put(neigh_comm, neigh_w);
					} else {
						var vbWeight:Box[double] = m.get(neigh_comm);
						neigh_w = neigh_w + vbWeight.value;
						m.remove(neigh_comm);
						m.put(neigh_comm, neigh_w);
					}
		        }								
			}

			g2.degrees(comm+1) = g2.degrees(comm)+m.size();
			g2.nb_links += m.size();

			set = m.keySet();
			it = set.iterator();
			var weight:Box[double];
			var node:int = 0;
			while(it.hasNext()) {
				node = it.next();
				weight = m.get(node);

				g2.total_weight += weight.value;
				links_tmp.add(node);
				weights_tmp.add(weight.value);
			}
			
		}
		g2.links  = links_tmp.toArray();
		g2.weights  = weights_tmp.toArray();
		links_tmp = null;
		weights_tmp = null;
		return g2;			
	}

	public def contractGraph() {
	//distributed graph data contraction
		if(role==0) Console.OUT.println("Contraction in parallel mode!!");
		var neigh:int = 0;
		var neigh_comm:int = 0;
		var neigh_w:double = 0.0;
		var renumber:Array[int] = new Array[int](g.nb_nodes+1, -1);
		var finalCount:int = 0;

		for (index in g.nodeList) {
			val node = g.nodeList(index);
			renumber(n2c(node)) = 0;
		}

		for (i in (0..g.nb_nodes)) {
			if (renumber(i)!=-1) {
				renumber(i)=finalCount;
				finalCount++;
			}
		}

		var comm_nodes:Array[ArrayList[int]] = new Array[ArrayList[int]](finalCount);
		var degreeIndexes:Array[ArrayList[int]] = new Array[ArrayList[int]](finalCount);

		finalCount = finalCount - 1;
		for (i in (0..finalCount)) {
			comm_nodes(i) = new ArrayList[int]();
			degreeIndexes(i) = new ArrayList[int]();
		}

		var indexes:Array[int] = new Array[int](finalCount+1); //store <index, communityID> info for making GraphBuff
		var nodeSize:int = g.nodeList.size-1;
		for (index in 0..nodeSize) {
			val node = g.nodeList(index);
			comm_nodes(renumber(n2c(node))).add(node);
			indexes(renumber(n2c(node))) = neigh_pos(n2c(node)); //store new node ID
			degreeIndexes(renumber(n2c(node))).add(index);
		}
		
		var g2:Graph = new Graph();
		g2.nb_nodes = comm_nodes.size;
		g2.degrees = new Array[int](comm_nodes.size);

		var comm_deg:int = comm_nodes.size-1;
		var set:Set[int];
		var it:Iterator[int];
		var links_tmp:ArrayList[int] = new ArrayList[int]();
		var weights_tmp:ArrayList[double] = new ArrayList[double]();

		for (comm in (0..comm_deg)) {
			val m = new HashMap[int, double]();
			var comm_size:int = comm_nodes(comm).size()-1;
			
			for ( node in (0..comm_size)) {
				val mainNode = comm_nodes(comm)(node);
		        val curComm = neigh_pos(n2c(mainNode));
				val degreeIndex = degreeIndexes(comm)(node);
				val startIndex = g.degrees(degreeIndex);
        		val deg  = g.degrees(degreeIndex+1) - 1;

		        for (i in (startIndex..deg)) {
					neigh = g.links(i);
					neigh_comm = neigh_pos(n2c(neigh));
					neigh_w = g.weights(i);

					if (curComm==neigh_comm) {
						val inside_w:double = g.weights(i);
					}		
					if(!m.containsKey(neigh_comm)) {
						m.put(neigh_comm, neigh_w);
					} else {
						var vbWeight:Box[double] = m.get(neigh_comm);
						neigh_w = neigh_w + vbWeight.value;
						m.remove(neigh_comm);
						m.put(neigh_comm, neigh_w);
					}
		        }								
			}

			g2.degrees(comm) = m.size();
			g2.nb_links += m.size();

			set = m.keySet();
			it = set.iterator();
			var weight:Box[double];
			var node:int = 0;
			while(it.hasNext()) {
				node = it.next();
				weight = m.get(node);

				g2.total_weight += weight.value;
				links_tmp.add(node);
				weights_tmp.add(weight.value);
			}
		}
		g2.links  = links_tmp.toArray();
		g2.weights  = weights_tmp.toArray();

		links_tmp = null;
		weights_tmp = null;

		return new GraphBuff(g2.degrees, g2.links, g2.weights, indexes, finalCount);			
	}
}