import x10.array.Array;

public class CommunityMessage {
	var n2cM:Array[int];
	var totM:Array[double];
	var insideM:Array[double];
	var commNewNumM:Array[int]; //store communities' new ID which will be used after constarct graph data 
	var sizeM:int=0;
	def this() {
		n2cM = new Array[int](1);
		totM = new Array[double](1);
		insideM = new Array[double](1);
		commNewNumM = new Array[int](1);
		sizeM = 0;
	}

	public def this(n2c:Array[int], tot:Array[double], inside:Array[double]) {
		n2cM = n2c;
		totM = tot;
		insideM = inside;
		commNewNumM = new Array[int](1);
	}
	
	public def this(n2c:Array[int], tot:Array[double], inside:Array[double], size:int, commNewNum:Array[int]) {
		n2cM = n2c;
		totM = tot;
		insideM = inside;
		sizeM = size;
		commNewNumM = commNewNum;
	}
}
