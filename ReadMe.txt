1.Compile or delete file using makefile
	- make
	- make clean

2.Convert txt graph data into binary data.

	-./convert -i inputfilename.txt -o outputfilename.bin -n maxnode 

  If the txt graph data is directed graph data, use option -d.

	-./convert -i inputfilename.txt -o outputfilename.bin -n maxnode -d 
  
#3.Convert txt graph data into binary data with prePartitioned file which is made from kMetis package
#    -It needs to specific partitioned filename( -pFile ), the number of partitions( -pNum ) 
#    	-./bin/convert -i input -o output -n maxnode -pFile prePartitionedFile -pNum partitionNumber

4.Calculate community 
	-sequential way
		-./bin/community  -i filename.bin -r
	-parallel way
		-mpirun -n 3 ./bin/community -i filename.bin -r -parallel

	-other options
		-pass n: only compute n passes per level
		-precision d: specify the precision of modularity
		-w : detect community with weighted graph data
		-heap: get the heap profile 
		-d: get the detail profile

