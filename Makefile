#!/bin/bash

CC=x10c++
EXEC=community convert testCopy test memory bcast testAH testTeam2 binary

CFLAGS=-J-Xmx2G -cxx-prearg  -I/home/usr5/14D55021/lib/scalegraph-2.2/include -sourcepath /home/usr5/14D55021/lib/scalegraph-2.2/src
FLAGS=-OPTIMIZE=true -VERBOSE -OPTIMIZE_COMMUNICATIONS=true -O -NO_CHECKS -define NO_BOUNDS_CHECKS  -cxx-prearg -g  -x10rt mpi   

all: $(EXEC)

community : src/ComDetect.x10
	$(CC) $(CFLAGS) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
convert : src/Convert.x10
	$(CC) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
testCopy : src/testCopy.x10
	$(CC) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
test : src/Test.x10
	$(CC) $(CFLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
memory : src/testMemory.x10
	$(CC) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
bcast : src/BcastTest.x10
	$(CC) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
testAH : src/BenchHashmapArray.x10
	$(CC) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
testTeam2 : src/TestTeam2.x10
	$(CC) $(CFLAGS) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
binary : src/testBinary.x10
	$(CC) $(CFLAGS) $(FLAGS) -o bin/$@ $^ 
	mv *.h obj/
	mv *cc obj/
install : all
	mv *.h obj/
	mv *cc obj/
	
##########################################
# Generic rules
##########################################

clean:
	rm -f *.cc *~ *.h bin/$(EXEC) obj/*h obj/*cc
